/* FIXME:
*
* export a function that adds a new element to the store.
*
* Rules:
* - add must be able to take either a single element
* or an array of new elements
* - you must use the functions from '../store'
*
*/

import {setState, getState} from '../store'

const add = (elem) => {
    let array = [...getState()] // creates the new array from the old one
    if (Array.isArray(elem)) // means is an array
    {
        array.forEach((element) => { // element = url so push an object with {id  = array length, url = element} 
            array.push({id: array.length, url: element});
        })
    }
    else
        array.push({id : array.length, url: elem});
    setState(array);
    return array;    
};

export default add;
