/* FIXME:
*
* export a function that updates a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
* - the updated element must not share the same reference as the previous one.
*
*/
import {getState, setState} from '../store'

const update = (id) => {
    let new_url = prompt("choose an url"); // to select a new url
    if (new_url == null)
        return;
    let arr = [...getState()];
    arr = arr.map(elem => (elem.id === id)? {id : elem.id, url : new_url} : elem)
    setState(arr);
    return arr;
};

export default update;
