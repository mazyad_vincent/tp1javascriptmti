/* FIXME:
*
* export a function that removes a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
*
*/

import {getState, setState} from '../store'

const remove = (id) => {
    let arr = [...getState()].filter(elem => elem.id !== id);

    let index = 0; // since we removed one element, we need to fix each id (otherwise id will be wrong)
    arr = arr.map(item => {
        let new_val = {id : index, url : item.url};
        index++;
        return new_val;
    });
    setState(arr);
    return arr;
};

export default remove;
