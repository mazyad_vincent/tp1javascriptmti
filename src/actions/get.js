/* FIXME:
*
* export a function that gets a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
*
*/

import {getState} from '../store'

const get = (id) => {
    let array = getState();
    return array.find((x => x.id === id)).url;
};

export default get;
